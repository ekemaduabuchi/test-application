﻿'use strict'
angular.module('testSchoolApp')
.controller('studentCtrl', ['$scope', 'studentResource', function ($scope, studentResource) {
    $scope.busy = true;
    $scope.students = studentResource.service.query(function (response) {
        $scope.busy = false;
    })
}])