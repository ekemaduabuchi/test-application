﻿'use strict'
angular.module('testSchoolApp')
.controller('registerCtrl', ['$scope', 'studentResource', function ($scope, studentResource) {
    $scope.busy = false;
    $scope.formData = {
        ProgramWrittenBy: 'Oriaku Eke Maduabuchi'
    };

    // register function here
    $scope.create = function () {
        $scope.busy = true;
        studentResource.service.save({}, $scope.formData, function (response) {
            // success 
            $scope.busy = false;
            $scope.formData = {};
            alert("Registration was successful");
            $scope.registerForm.$setPristine();
            $scope.registerForm.$setUntouched();
        },
        //error
        function (error) {
            $scope.busy = false;
            alert(error.statusText)
        })
    }

}])