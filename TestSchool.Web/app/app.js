﻿var schoolApp = angular.module('testSchoolApp', ['ui.router', 'common.services', 'ngMessages'])

.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
.state('home', {
    url: '/home',
    abstract: true,
    templateUrl: 'app/templates/masterPage.html',
})
 .state('home.newstudent', {
     url: '/new-student',
     views: {
         'bodyContent': {
             templateUrl: 'app/templates/new-student.html',
             controller: 'registerCtrl'
         }
     }
 })
 .state('home.viewstudent', {
     url: '/view-student',
     views: {
         'bodyContent': {
             templateUrl: 'app/templates/view-students.html',
             controller: 'studentCtrl'
         }
     }
 })

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/home/new-student');
})

