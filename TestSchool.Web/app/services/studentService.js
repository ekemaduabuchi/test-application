﻿'use strict'
angular.module('common.services')
.factory('studentResource', ['$resource', 'appSettings', function ($resource, appSettings) {
    return {
        service: $resource(appSettings.serverPath + "api/student/:id", { id: '@id' }, {
            'update': { method: 'POST' },
        }),
    };
}])