﻿(function () {
	'use-strict'
	angular.module("common.services", ['ngResource'])
    .constant("appSettings", {
    	// Your localhost Url here
    	serverPath: "http://localhost:1114/"
    })
}());