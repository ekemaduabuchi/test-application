﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestSchool.API.Models
{
    public class StudentViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DOB { get; set; }
        public string Department { get; set; }
        public string Level { get; set; }
        public string ProgramWrittenBy { get; set; }
    }
}
