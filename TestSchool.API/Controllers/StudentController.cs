﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using TestSchool.API.DataAccess;
using TestSchool.API.Models;

namespace TestSchool.API.Controllers
{
    public class StudentController : ApiController
    {
        private TestSchoolContext _context;
        protected TestSchoolContext context
        {
            get
            {
                if (_context == null) _context = new TestSchoolContext();
                return _context;
            }
        }

        public async Task<IHttpActionResult> Post(Student model)
        {
            if (model.DOB == null) model.DOB = DateTime.UtcNow;
            if (ModelState.IsValid)
            {
                context.Student.Add(model);
                await context.SaveChangesAsync();
                return Ok();
            }
            return Content(HttpStatusCode.InternalServerError, "Error!!! Please try again");
        }

        public async Task<IHttpActionResult> Get()
        {
            var students = context.Student.ToList();
            if (students.Count() > 0)
            {
                return Ok(students);
            }
            return Content(HttpStatusCode.NoContent, "No Student Record found");
        }
    }
}
