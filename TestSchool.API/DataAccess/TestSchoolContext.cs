﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
using TestSchool.API.Models;

namespace TestSchool.API.DataAccess
{
    public class TestSchoolContext : DbContext
    {
        public TestSchoolContext(): base("name=TestSchoolConnection")
        {
        }
        public DbSet<Student> Student { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}